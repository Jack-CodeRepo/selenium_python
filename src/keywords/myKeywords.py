#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""my_keywords.py

    Contient des fonctions simples pour tester des elements du DOM
    Tentative d'imiter les keywords robot framework

"""


# ==================================================================================================
#   IMPORT STANDARD LIBRARIES AND MODULES
# ==================================================================================================

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By



# ==================================================================================================
#   IMPORT CUSTOM LIBRARIES AND MODULES
# ==================================================================================================

from src.browsers.myBrowser import MyBrowser
from src.core.logging import Logging
from src.core.tags import Tagger



# ==================================================================================================
#   FONCTIONS
# ==================================================================================================

# def aller_sur_une_url(my_driver, my_url):
#     """naviguer vers une url

#     Args:
#         my_driver (webdriver): l'instance du webdriver utilisée
#         my_url (str): url vers laquelle naviguer
#     """
#     try:
#         my_driver.get(my_url)
#         logging.info(f"{Tagger.INFO} url cible: {my_url}")
#     except:
#         logging.warning(f"{Tagger.FAIL} impossible de naviguer vers {my_url}.")



# def appuyer_sur_une_touche(my_element, my_key):
#     """simmuler la pression d'une touche du clavier

#     Args:
#         my_element (webdriver): l'element cible
#         my_key (str): la touche du clavier à simuler
#     """
#     if my_key == "RETURN":
#         try:
#             my_element.get_element().send_keys(Keys.RETURN)
#             logging.info(f"{Tagger.SUCCESS} Touche simulée: RETURN")
#         except:
#             logging.warning(f"{Tagger.FAIL} impossible de simuler la touche RETURN")
#     else:
#         logging.warning(f"{Tagger.FAIL} touche {my_key} non supportee.")


def creer_le_webdriver(driver_name: str):
    try:
        driver = MyBrowser(driver_name).driver
        Logging.log(f"{Tagger.INFO} webdriver créé")
    except Exception:
        Logging.log(f"{Tagger.FAIL} impossible de créer le webdriver.")



def fermer_le_webdriver():
    """Ferme le webdriver

    Args:
        my_driver (webdriver): l'instance du webdriver utilisée
    """
    try:
        driver = MyBrowser("any").driver
        Logging.log(f"{Tagger.INFO} webdriver fermé.")
    except Exception:
        Logging.log(f"{Tagger.FAIL} impossible de fermer le webdriver.")
