#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""pj_acceuil.py

    Tentative de Page Object Model

    Attributs:
        MY_DRIVER:  le driver utilisé par le test

    Methodes:
        Chaques méthodes renvois un élément du DOM
"""



# ==================================================================================================
#   IMPORT STANDARD LIBRARIES AND MODULES
# ==================================================================================================



# ==================================================================================================
#   IMPORT CUSTOM LIBRARIES AND MODULES
# ==================================================================================================

from src.browsers.myBrowser import MyBrowser
from src.core.element import Element
from src.core.logging import Logging
from src.core.tags import Tagger




# ==================================================================================================
#   CLASS
# ==================================================================================================

class pj_acceuil():
    """pj_acceuil

        tentative de créer une POM de la page d'acceuil des pages-jaunes
    """
    instanceCnt = 0

    def __init__(self):
        self.__driver = MyBrowser("any").driver
        self.url = "https://devmarboeuf.fr/repos/Jack-CodeRepo"
        self.__driver.get(self.url)

        self.explorer = Element("xpath", "//a[@href='/repos/explore/repos']")

# --------------------------------------------------------------------------------------------------
#	METHODES
# --------------------------------------------------------------------------------------------------
    
    def aller_sur_la_page(self):
        """naviguer vers une url

        Args:
            my_driver (webdriver): l'instance du webdriver utilisée
            my_url (str): url vers laquelle naviguer
        """
        try:
            self.__driver.get(self.url)
            Logging.log(f"{Tagger.SUCCESS} Go to {self.url}")
        except:
            Logging.log(f"{Tagger.FAIL} Go to {self.url}")


    def cliquer_sur_explorateur(self):
        self.explorer.click()














    