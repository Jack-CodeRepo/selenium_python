

class MetaSingleton(type):

    # dictionnary containing all instanciated singleton
    __instance = {}

    def __call__(cls, *args, **kwds):
        if cls not in cls.__instance:
            instance = super().__call__(*args, **kwds)
            cls.__instance[cls] = instance
        return cls.__instance[cls]


    def list_all_instances(cls):
        if cls.__instance:
            print("All instanciated singletons are:")
            for cnt, value in enumerate(cls.__instance):
                print(f"- Instance {cnt + 1} is: {value},    hex memory adress is: {hex(id(value))}")