#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""myBrowser.py
Returns:
    [type]: [description]
"""


# ==================================================================================================
#   IMPORT STANDARD LIBRARIES AND MODULES
# ==================================================================================================

import json
import logging
from pathlib import Path
from sys import path
import platform

from selenium.webdriver import Firefox, FirefoxOptions
from selenium.webdriver import Chrome, ChromeOptions

from src.browsers.design_patterns import MetaSingleton


# ==================================================================================================
#   IMPORT CUSTOM LIBRARIES AND MODULES
# ==================================================================================================

from src.core.tags import Tagger



# ==================================================================================================
#   VARIABLES
# ==================================================================================================

logLevel = False

my_os = platform.system().lower()

root_folder = path[0]

# ==================================================================================================
#   INIT USED DATA
# ==================================================================================================

browser_data_file = f"{Path(__file__).parent}/settings.json"
try:
    with open(browser_data_file) as f:
        browser_data = json.load(f)
except:
    logging.warning(f"origin: {Path(__file__)}")
    logging.warning(f"No settings file detected (file: {browser_data_file})")
    exit()



# ==================================================================================================
#   CLASS
# ==================================================================================================

class MyBrowser(metaclass=MetaSingleton):
    def __init__(self, name):
        self.__name = str(name).lower()

        self.driver = self.generate_driver()


# --------------------------------------------------------------------------------------------------
    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.__name = value



# ==================================================================================================
#   FONCTIONS
# ==================================================================================================

    def generate_driver(self):
        """genere un webdriver

        Returns:
            webdriver: le webdriver
        """
        if self.name in browser_data["settings"]["BROWSERS"]:
            for name in browser_data["settings"]["BROWSERS"]:
                if self.name in name:
                    logging.info(f"{Tagger.INFO} generated driver: {self.name}")
                    return self.__get_driver()

        else:
            logging.warning(f"{Tagger.FAIL} unsuported driver: {self.name}")



    def __get_driver(self):
        """genere un webdriver et set ses options

        Returns:
            webdriver: le webdriver
        """
       
        if self.name == "firefox":
            return Firefox(options=self.__set_options())
        elif self.name == "chrome":
            return Chrome(options=self.__set_options())



    def __set_options(self):
        """genere les options d'un webdriver à partir d'un fichier json

        Returns:
            list: une liste d'options
        """

        if self.name == "firefox":
            browser_options = FirefoxOptions()
        elif self.name == "chrome":
            browser_options = ChromeOptions()

        for option in browser_data["settings"]["BROWSERS"][f"{self.name}"]["OPTIONS"]:
            if not option:
                continue
            else:
                browser_options.add_argument(f"--{option}")
                logging.info(f"{Tagger.INFO} option setted: {option}")
        return browser_options

