#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================



from src.browsers.myBrowser import MyBrowser
from src.core.logging import Logging
from src.core.tags import Tagger


class Element:

    def __init__(self, my_type:str, my_expr:str):
        """Element

        Args:
            my_driver (webdriver): le webdrvier à utiliser pour générer/trouverl'element
            my_type (str): le nom du type à trouver.
                types supportés: id, xpath
            my_expr (str): expression à chercher
        """
        self.__driver = MyBrowser("any").driver
        self.__elem_type = str(my_type).lower()
        self.__elem_expr = my_expr
        self.__elem_str = f"{self.elem_type} : {self.elem_expr}"

# --------------------------------------------------------------------------------------------------
    @property
    def driver(self):
        return self.__driver

    @driver.setter
    def driver(self, value):
        self.__driver = value

# --------------------------------------------------------------------------------------------------
    @property
    def elem_type(self):
        return self.__elem_type

    @elem_type.setter
    def elem_type(self, value):
        self.__elem_type = value

# --------------------------------------------------------------------------------------------------
    @property
    def elem_expr(self):
        return self.__elem_expr

    @elem_expr.setter
    def elem_expr(self, value):
        self.__elem_expr = value

# --------------------------------------------------------------------------------------------------
    @property
    def elem_str(self):
        return self.__elem_str

    @elem_str.setter
    def elem_str(self, value):
        self.__elem_str = value



# --------------------------------------------------------------------------------------------------
#	METHODES
# --------------------------------------------------------------------------------------------------

    def get_element(self, a_faill=False):
        if self.elem_type == "id":
            try:
                return self.driver.find_element_by_id(self.elem_expr)
            except:
                self.__elem_not_found()

        elif self.elem_type == "xpath":
            try:
                return self.driver.find_element_by_xpath(self.elem_expr)
            except:
                self.__elem_not_found(a_faill)



    def __elem_not_found(self, a_faill):
        Logging.log(f"{Tagger.FAIL} element not found {self.elem_str}", a_faill=False)
        self.driver.close()
        exit(0)



    def click(self):
        try:
            self.get_element().click()
            Logging.log(f"{Tagger.SUCCESS} Click on {self.elem_str} successfull")
        except Exception:
            Logging.log(f"{Tagger.FAIL} Could not click on element {self.elem_str}")



    def input_text(self, a_text: str):
        try:
            self.get_element().send_keys(a_text)
            Logging.log(f"{Tagger.SUCCESS} Input text '{a_text}' on element {self.elem_str} {Tagger.SUCCESS}")
        except Exception:
            Logging.log(f"{Tagger.FAIL} Input text '{a_text}' on element {self.elem_str} {Tagger.FAIL}")
