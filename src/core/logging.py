import logging
from datetime import datetime
from sys import path



root_folder     = path[0]


from src.core.tags import Tagger


timeFormat      = "%Y_%m_%d_%H%M%S"


logging.basicConfig(level=logging.INFO,
                    filename=f"{root_folder}/log/{datetime.now().strftime(timeFormat)}_mykeywords.log",
                    format='%(asctime)s_%(levelname)s %(message)s'
                    )





class Logging:

    def log(msg: str, a_faill = False):
        logging.info(msg)
        
        if a_faill:
            exit(0)













