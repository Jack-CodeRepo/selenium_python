#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""Tagger

    Simple class pour gérer les tags utilisés dans les log_messages
    Pas d'instanciation nécessaire
"""


class Tagger:
    INFO = "[INFO]"
    FAIL = "[FAIL]"
    SUCCESS = "[SUCESS]"
