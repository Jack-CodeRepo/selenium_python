#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""
    context.py

    - ajoute le dossier racine dans le PATH
    - gere les imports utilisés par les tests
        Toute ressources utilisées par un test doit etre importées ici

    liste des ressources importées
        src.keywords.myKeywords ::  *
        src.browsers.myBrowser  ::  MyBrowser   (class)
        src.pages.pj_acceuil    ::  pj_acceuil  (class)
        src.misc.tags           ::  Tagger      (class)


"""

# ==================================================================================================
#   IMPORT STANDARD LIBRARIES AND MODULES
# ==================================================================================================

from os.path import abspath, dirname, join
from sys import path



# add root folder to PATH
path.insert(0, abspath(join(dirname(__file__), "..")))



# ==================================================================================================
#   IMPORT CUSTOM LIBRARIES AND MODULES
# ==================================================================================================

from src.keywords.myKeywords import *
from src.browsers.myBrowser import MyBrowser
from src.pages.pj_acceuil import pj_acceuil
from src.core.logging import Logging


