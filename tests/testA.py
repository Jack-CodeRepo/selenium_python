#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""
    main.py

    simple test de la page d'acceuil des pages jaunes

    parfois le test fail car il rencontre la sécurité parant l'utilisation abusive de robots
    
    tentative de:
        - une class qui gere les locator de la page d'acceuil  src/pages/pj_acceuil.py
        - redaction de mot clefs    src/keywords/myKeywords.py

"""



# ==================================================================================================
#   IMPORT STANDARD LIBRARIES AND MODULES
# ==================================================================================================

import json
from datetime import datetime

from sys import path



# ==================================================================================================
#   IMPORT CUSTOM LIBRARIES AND MODULES
# ==================================================================================================

# context importe les modules principaux contenus dans src/
from context import *



# ==================================================================================================
#   VARIABLES
# ==================================================================================================

root_folder = path[0]



# ==================================================================================================
#   INIT USED DATA
# ==================================================================================================

dataFile = f"{root_folder}/ressources/jdd/jdd01.json"

try:
    with open(dataFile) as f:
        data = json.load(f)
except:
    print(f"No test data_file file detected (file: {dataFile})")
    exit()

driver_name = "Firefox"



# ==================================================================================================
#   GENERATION DU DRIVER
# ==================================================================================================

creer_le_webdriver("firefox")



# ==================================================================================================
#   INIT DES PAGES UTILISEES
# ==================================================================================================

page_accueil = pj_acceuil()


# ==================================================================================================
#   TESTS
# ==================================================================================================

page_accueil.aller_sur_la_page()

page_accueil.cliquer_sur_explorateur()


# ==================================================================================================
#   FERMETURE DU DRIVER
# ==================================================================================================

fermer_le_webdriver()



