#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

from pathlib import Path
from os import system


def purge_log():
    logfolderpath=Path(f"{Path(__file__).parent}/../log/")

    file_list = list()
    cnt = 0
    for file in logfolderpath.iterdir():
        if "log" in file.name:
            cnt += 1
            file_list.append(file)
    file_list.sort()

    if cnt == 5:
        print(f"Compteur log: {cnt}. Pas de purge à faire.")
        exit()

    for file in file_list:
        if cnt > 5:
            cnt -= 1
            system(f"rm {file}")


if __name__ == "__main__":
    purge_log()